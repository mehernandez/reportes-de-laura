import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import * as XLSX from 'xlsx';

import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  providers: [NgbModalConfig, NgbModal],
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor(config: NgbModalConfig, private service: ServicesService) { }

  reporte: any;
  reportes = [];
  report={"id":"","nombre":"","apellido":"", "empresa":"", "cargo":"", "edad":""}

  ngOnInit(): void {
    this.service.getReports().subscribe(reportes => {
      this.reportes = reportes;
    });
  }

  abrirFormulario(reporte) {
    this.report = reporte;
  }

  descargar(element:string) {
    var data = document.getElementById(element);
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 100;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('tabla.pdf'); // Generated PDF   
    });
  }

  exportexcel(): void {
    /* pass here the table id */
    let element = document.getElementById('tabla');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, "tabla.xlsx");
  }

}
